package activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import mytweet.org.wit.mytweet.R;

/**
 * Created by ictskills on 11/10/16.
 *
 * Part of the timeline fragment, creates the fragment manager to then call the new timeline fragment which contains the methods.
 */
public class TimelineActivity extends AppCompatActivity{

        public void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.fragment_container);

            FragmentManager manager = getSupportFragmentManager();
            Fragment fragment = manager.findFragmentById(R.id.fragmentContainer);
            if (fragment == null)
            {
                fragment = new TimelineFragment();
                manager.beginTransaction().add(R.id.fragmentContainer, fragment).commit();
            }
        }
    }
