package activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import app.myTweetApp;
import models.Portfolio;
import models.User;
import mytweet.org.wit.mytweet.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Signup extends AppCompatActivity implements Callback<User> {

    private myTweetApp app;
    private Portfolio portfolio;

    //Initialises the register buttons
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        app = (myTweetApp) getApplication();
        portfolio = app.portfolio;

    }

    public void registerPressed(View view)
    {
        TextView firstName = (TextView) findViewById(R.id.signUpFirstname);
        TextView lastName = (TextView) findViewById(R.id.signUpLastname);
        TextView email = (TextView) findViewById(R.id.Email);
        TextView password = (TextView) findViewById(R.id.Password);

//        portfolio.selectUsers();
        User user = new User(firstName.getText().toString(), lastName.getText().toString(), email.getText().toString(), password.getText().toString());
      //  portfolio.addUser(user);

        //Call is made so users can login with credentials from node app.
        myTweetApp app = (myTweetApp) getApplication();
        Call<User> call = (Call<User>) app.tweetService.createUser(user);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<User> call, Response<User> response) {
        Toast toast = Toast.makeText(this, "Signup Successful", Toast.LENGTH_LONG);
        toast.show();
        //app.users.add(response.body());
        startActivity(new Intent(this, Login.class));

    }

    @Override
    public void onFailure(Call<User> call, Throwable t) {
        app.tweetServiceAvailable = false;
        Toast toast = Toast.makeText(this, "MyTweet Service Unavailable. Try again later", Toast.LENGTH_LONG);
        toast.show();
        startActivity (new Intent(this, myTweetWelcome.class));
    }
}


