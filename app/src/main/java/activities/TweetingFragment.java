package activities;
import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.helpers.ContactHelper;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v13.app.FragmentCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
//import android.support.v7.app.AppCompatActivity;

import models.Tweet;
import models.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.app.Fragment;
import android.widget.Toast;


import java.util.List;

import app.myTweetApp;
import models.Portfolio;
import mytweet.org.wit.mytweet.R;


import static android.helpers.CameraHelper.showPhoto;
import static android.helpers.ContactHelper.sendEmail;
import static android.helpers.IntentHelper.navigateUp;

public class TweetingFragment extends Fragment implements TextWatcher,
        CompoundButton.OnCheckedChangeListener,
        View.OnClickListener {


    private EditText tweetBox;
    private TextView charCount;
    private Button sendTweet;
    private Portfolio portfolio;
    private Tweet tweet;
    private TextView dateTime;
    private Button tweetRecipient;
    private Button emailTweet;
    private String emailAddress = " ";
    private Intent data;


    // private int countDown = 140;.
    protected static myTweetApp app;

    private static final int REQUEST_PHOTO = 0;

    public static final String EXTRA_TWEET_ID = "TWEET_ID";

    private static final int REQUEST_CONTACT = 1;

    private ImageView cameraButton;
    private ImageView photoView;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        String twtId = (String) getArguments().getSerializable(EXTRA_TWEET_ID);

        // Long twtId = (Long) getArguments().getSerializable(EXTRA_TWEET_ID);

        app = myTweetApp.getApp();
        portfolio = app.portfolio;
        tweet = portfolio.getTweet(twtId);


    }


    //Method implements the view for when the tweeting page is accessed.
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        super.onCreateView(inflater, parent, savedInstanceState);
        View v = inflater.inflate(R.layout.activity_tweeting, parent, false);
        dateTime = (TextView) v.findViewById(R.id.dateTime);
        dateTime.setText(tweet.getDateString());

        addListeners(v);
        if (tweet.content != null) {
            updateControls(tweet);
        }
        return v;
    }


    //Method to initialise the buttons and text/edit views and set listeners on them.
    private void addListeners(View v) {

        tweetBox = (EditText) v.findViewById(R.id.tweetBox);
        charCount = (TextView) v.findViewById(R.id.charCount);
        sendTweet = (Button) v.findViewById(R.id.sendTweet);
        tweetRecipient = (Button) v.findViewById(R.id.tweetRecipient);
        emailTweet = (Button) v.findViewById(R.id.emailTweet);
        cameraButton = (ImageView) v.findViewById(R.id.camera_button);
        photoView = (ImageView) v.findViewById(R.id.tweetImage);

        tweetBox.addTextChangedListener(this);
        charCount.setOnClickListener(this);
        sendTweet.setOnClickListener(this);
        tweetRecipient.setOnClickListener(this);
        emailTweet.setOnClickListener(this);
        cameraButton.setOnClickListener(this);

    }

    //Called within the on createView if the content is not null and makes them read-only.
    public void updateControls(Tweet tweet) {
        tweetBox.setText(tweet.content);
        dateTime.setText(tweet.getDateString());
        dateTime.setEnabled(false);
        tweetBox.setEnabled(false);
    }

    @Override
    public void onStart() {
        super.onStart();
        //display thumbnail photo
        showPhoto(getActivity(), tweet, photoView);
    }


    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    //Method to change the character counter as a user types into the tweetbox.
    @Override
    public void onTextChanged(CharSequence s, int i, int i1, int i2) {
        int length = s.length();
        int max = 140;
        String convert = ("" + (max - length));
        //String convert = String.valueOf(s.length());
        charCount.setText(convert);
    }

    @Override
    public void afterTextChanged(Editable editable) {
        tweet.setContent(editable.toString());
    }

    //Switch statement to control what happens when buttons are clicked within the tweet view.
    @Override
    public void onClick(View view)

    {
        //Getting the text that was in the tweetbox
        String tweetbox = this.tweetBox.getText().toString();
        switch (view.getId()) {

            case R.id.camera_button:
                Intent ic = new Intent(getActivity(), TweetCameraActivity.class);
                startActivityForResult(ic, REQUEST_PHOTO);
                break;

            //If the user wants to send the tweet to a contact on their device.
            case R.id.tweetRecipient:
                Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(i, REQUEST_CONTACT);
                break;

            //Emails the tweet and place it in the mail app using a helper class.
            case R.id.emailTweet:
                sendEmail(getActivity(), emailAddress,
                        getString(R.string.tweet_subject), getTweetTimleine(getActivity()));
                break;


            //Sends the tweet unless it is null and then starts the timeline activity with the new tweet displaying.
            case R.id.sendTweet:
                if (tweetbox.equals("")) {
                    portfolio.tweets.remove(tweet);
                    portfolio.updateTweets(tweet);
                    Toast toast = Toast.makeText(getActivity(), "Tweet not sent!", Toast.LENGTH_SHORT);
                    toast.show();
                }

                startActivity(new Intent(getActivity(), TimelineActivity.class));
                /*Toast toast = Toast.makeText(getActivity(), "Tweet Sent!", Toast.LENGTH_SHORT);
                toast.show();*/


        }
    }


    //Checks if the user has allowed permissions for the app to access contacts.
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        {
            if (resultCode != Activity.RESULT_OK) {
                return;
            }
            switch (requestCode) {
                case REQUEST_CONTACT:
                    this.data = data;
                    checkContactsReadPermission();
                    break;
                case REQUEST_PHOTO:
                    String filename = data.getStringExtra(TweetCameraActivity.EXTRA_PHOTO_FILENAME);
                    if (filename != null) {
                        tweet.photo = filename;
                        showPhoto(getActivity(), tweet, photoView);
                    }
                    break;
            }
        }
        /*switch (requestCode)
        {
            case REQUEST_CONTACT:
                this.data = data;
                checkContactsReadPermission();
                break;

        }*/
    }

    /*If the user wants to choose a contact the information is extracted using this method.
    Adds the information of the contact onto the button to be displayed.*/
    private void readContact() {
        String name = ContactHelper.getContact(getActivity(), data);
        emailAddress = ContactHelper.getEmail(getActivity(), data);
        tweetRecipient.setText(name + " : " + emailAddress);

    }



    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

    }


    /*Method to use the up button to leave the tweet activity page and go back to the timeline.
     No blank tweets allowed.
    So creates it and then removes it from the portfolio.*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                navigateUp(getActivity());
                String tweetbox = this.tweetBox.getText().toString();
                if (tweetbox.equals("")) {
                    portfolio.tweets.remove(tweet);

                    Call<Tweet> call2 = app.tweetService.deleteTweet(tweet);
                    call2.enqueue(new Callback<Tweet>() {
                        @Override
                        public void onResponse(Call<Tweet> call, Response<Tweet> response) {
                            portfolio.updateTweets(tweet);
                        }

                        @Override
                        public void onFailure(Call<Tweet> call, Throwable t) {
                            Toast.makeText(getActivity(), "Tweet not sent", Toast.LENGTH_LONG).show();
                        }

                    });

                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();
        portfolio.updateTweets(tweet);

    }


    public String getTweetTimleine(Context context) {
        String TweetBox = this.tweetBox.getText().toString();
        String report = TweetBox;
        return report;

    }



    /**
     * http://stackoverflow.com/questions/32714787/android-m-permissions-onrequestpermissionsresult-not-being-called
     * This is an override of FragmentCompat.onRequestPermissionsResult
     *
     * @param requestCode  Example REQUEST_CONTACT
     * @param permissions  String array of permissions requested.
     * @param grantResults int array of results for permissions request.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CONTACT: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    readContact();
                }
                break;
            }
        }
    }


    /**
     * Bespoke method to check if read contacts permission exists.
     * If it exists then the contact sought is read.
     * Otherwise, the method FragmentCompat.request permissions is invoked and
     * The response is via the callback onRequestPermissionsResult.
     * In onRequestPermissionsResult, on successfully being granted permission then the sought contact is read.
     */
    private void checkContactsReadPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {

            readContact();
        } else {
            // Invoke callback to request user-granted permission
            FragmentCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.READ_CONTACTS},
                    REQUEST_CONTACT);
        }
    }


    /*@Override
    public void onResponse(Call<Tweet> call, Response<Tweet> response) {
        portfolio.tweets.add(response.body());
        Toast toast = Toast.makeText(getActivity(), "Tweet has been sent!",Toast.LENGTH_LONG);
        toast.show();
    }

    @Override
    public void onFailure(Call<Tweet> call, Throwable t) {
        Toast toast = Toast.makeText(getActivity(), "Error, Tweet has not been sent", Toast.LENGTH_LONG);
        toast.show();
    }*/

}
