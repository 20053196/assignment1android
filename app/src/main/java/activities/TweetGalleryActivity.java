package activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ImageView;

import app.myTweetApp;
import models.Portfolio;
import models.Tweet;
import mytweet.org.wit.mytweet.R;

import static android.helpers.CameraHelper.showPhoto;

/**
 * Created by ictskills on 14/12/16.
 */
public class TweetGalleryActivity extends AppCompatActivity {

    public static final String EXTRA_PHOTO_FILENAME = "mytweet.activities.photo.filename";
    private ImageView photoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gallery);
        photoView = (ImageView) findViewById(R.id.galleryImage);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        showPicture();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showPicture() {
        {
            String twtId = (String) getIntent().getSerializableExtra(TweetingFragment.EXTRA_TWEET_ID);
            myTweetApp app = (myTweetApp) getApplication();
            Portfolio portfolio = app.portfolio;
            Tweet tweet = portfolio.getTweet(twtId);
            showPhoto(this, tweet, photoView);
        }

    }
}
