package activities;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import app.MyTweetService;
import app.myTweetApp;
import models.Portfolio;
import models.Tweet;
import models.User;
import mytweet.org.wit.mytweet.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
//A class to allow the user to login to the app.

public class Login extends AppCompatActivity implements View.OnClickListener
{

    private Button loginButton;
    public EditText email;
    public EditText password;
    private myTweetApp app;


    //Initializes the fields that will be needed within the view.
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginButton = (Button) findViewById(R.id.loginButton);
        loginButton.setOnClickListener(this);
        email = (EditText) findViewById(R.id.Email);
        password = (EditText) findViewById(R.id.Password);
        app = (myTweetApp) getApplication();
    }

    //Method to navigate once the buttons are pressed.

    @Override
    public void onClick(View view) {


        email     = (EditText)  findViewById(R.id.Email);
        password  = (EditText)  findViewById(R.id.Password);


        if (app.validUser(email.getText().toString(), password.getText().toString()))
        {
            startActivity(new Intent(this, TimelineActivity.class));

        }
        else
        {
            Toast toast = Toast.makeText(this, "Invalid Credentials", Toast.LENGTH_SHORT);
            toast.show();

        }
    }


/*    @Override
    public void onResponse(Call<User> call, Response<User> response) {

    }

    @Override
    public void onFailure(Call<User> call, Throwable t) {

    }*/
}

