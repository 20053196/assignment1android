package activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import android.widget.Button;
import android.widget.Toast;


import java.util.List;

import app.myTweetApp;
import models.Tweet;
import models.User;
import mytweet.org.wit.mytweet.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class myTweetWelcome extends AppCompatActivity implements Callback<List<User>>  {

    private Button signUpButton;
    private Button loginButton;

    private myTweetApp app;

//Initialises the login and signup buttons on the my tweet welcome activity.

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_tweet_welcome);
        app = (myTweetApp) getApplication();
    }

    //API call to get a list of users and tweets to render in the timeline view.
    @Override
    public void onResume()
    {
        super.onResume();
        app.currentUser = null;
        Call<List<User>> call1 = (Call<List<User>>) app.tweetService.getAllUsers();
        call1.enqueue(this);
        /*Call<List<Tweet>> call2 = (Call<List<Tweet>>) app.tweetService.getAllTweets();
        call2.enqueue(new Callback<List<Tweet>>() {
            @Override
            public void onResponse(Call<List<Tweet>> call, Response<List<Tweet>> response) {
                app.tweets = response.body();
            }
            @Override
            public void onFailure(Call<List<Tweet>> call, Throwable t) {
                app.tweetServiceAvailable = false;
                serviceUnavailableMessage();
            }
        });*/
    }
    //The api service form the node is made available.
    @Override
    public void onResponse(Call<List<User>> call, Response<List<User>> response)
    {
        serviceAvailableMessage();
        app.users = response.body();
        app.tweetServiceAvailable = true;
    }

    //If the call fails then the display a message to notify the user.
    @Override
    public void onFailure(Call<List<User>> call, Throwable t)
    {
        app.tweetServiceAvailable = false;
        serviceUnavailableMessage();
    }

    //Action for when the login is pressed bring user to login page.
    public void loginPressed (View view)
    {
        if (app.tweetServiceAvailable)
        {
            startActivity (new Intent(this, Login.class));
        }
        else
        {
            serviceUnavailableMessage();
        }
    }

    //Brings user to signup page.
    public void signUpPressed (View view)
    {
        if (app.tweetServiceAvailable)
        {
            startActivity (new Intent(this, Signup.class));
        }
        else
        {
            serviceUnavailableMessage();
        }
    }

    //Message for when the service is unavailable.
    void serviceUnavailableMessage()
    {
        Toast toast = Toast.makeText(this, "MyTweet Service Unavailable. Try again later", Toast.LENGTH_LONG);
        toast.show();
    }
    //Message for the the service is available and can be contacted.
    void serviceAvailableMessage()
    {
        Toast toast = Toast.makeText(this, "MyTweet Contacted Successfully", Toast.LENGTH_LONG);
        toast.show();
    }

}
