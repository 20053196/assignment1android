package activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.helpers.IntentHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.support.v4.app.ListFragment;
import java.util.ArrayList;
import java.util.List;


import android.widget.ArrayAdapter;
import android.widget.ListView;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.content.Intent;
import android.os.Bundle;


import models.Tweet;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import settings.SettingsActivity;
import app.myTweetApp;
import models.Portfolio;
import mytweet.org.wit.mytweet.R;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;
import android.widget.AbsListView;
import android.view.ActionMode;
import android.widget.Toast;

/**
 * Created by ictskills on 11/10/16.
 */
public class TimelineFragment extends ListFragment implements Callback<List<Tweet>>, OnItemClickListener, AbsListView.MultiChoiceModeListener {

    // private ArrayList<Tweet> tweets;
    private Portfolio portfolio;
    public TimelineAdapter adapter;
    myTweetApp app;
    private ListView listView;

    //Initialises fields.

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().setTitle(R.string.app_name);

        app = myTweetApp.getApp();
        portfolio = app.portfolio;
        //tweets = portfolio.tweets;

        adapter = new TimelineAdapter(getActivity(), portfolio.tweets);
        setListAdapter(adapter);

        Call<List<Tweet>> call = (Call<List<Tweet>>)app.tweetService.getAllTweets();
        call.enqueue(this);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        ((TimelineAdapter)getListAdapter()).notifyDataSetChanged();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, parent, savedInstanceState);

        listView = (ListView) v.findViewById(android.R.id.list);
        listView.setAdapter(adapter);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView.setMultiChoiceModeListener(this);


        return v;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Tweet tweet = ((TimelineAdapter) getListAdapter()).getItem(position);
        Intent i = new Intent(getActivity(), TweetingPagerActivity.class);
        i.putExtra(TweetingFragment.EXTRA_TWEET_ID, tweet._id);
        startActivityForResult(i, 0);
    }

    //Creates the timeline list menu.
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.timelinelist, menu);
    }

    //Gives the options to add a new tweet through the plus button, clear or start settings menu.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_new_tweet:
                Tweet tweet = new Tweet();
                createTweet(tweet);

                // call method to store tweet in app

                return true;

            case R.id.action_settings:
                startActivity(new Intent(getActivity(), SettingsActivity.class));
                return true;

            case R.id.menu_app_clear:
                listView.setAdapter(null);
                portfolio.clearTweets();

            case R.id.action_refresh:
                retrieveTweets();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void createTweet(Tweet tweet) {
        Call<Tweet> call = (Call<Tweet>) app.tweetService.createTweet(myTweetApp.currentUser._id, tweet);
        call.enqueue(new Callback<Tweet>() {
            @Override
            public void onResponse(Call<Tweet> call, Response<Tweet> response) {
                Tweet tweet = response.body();
                if (tweet != null) {
                    portfolio.addTweet(tweet);
                    //portfolio.tweets.add(response.body());
                    Intent i = new Intent(getActivity(), TweetingPagerActivity.class);
                    i.putExtra(TweetingFragment.EXTRA_TWEET_ID, tweet._id);
                    startActivityForResult(i, 0);

                }
                else {
                    Toast.makeText(getActivity(), "Cannot create tweet", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Tweet> call, Throwable t) {
                Toast toast = Toast.makeText(getActivity(), "Error, Tweet has not been sent", Toast.LENGTH_LONG);
                toast.show();
            }
        });
    }


    public void retrieveTweets() {
        RetrieveTweets retrieveTweets = new RetrieveTweets();
        Call<List<Tweet>> call = app.tweetService.getAllTweets();
        call.enqueue(retrieveTweets);
        Toast.makeText(getActivity(), "Retrieving Tweet list", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        Tweet tweet = adapter.getItem(position);
        IntentHelper.startActivityWithData(getActivity(), TweetingFragment.class, "TWEET_ID", tweet._id);

    }

    // Method implemented with onclick listener interface but not needed.

    @Override
    public void onItemCheckedStateChanged(ActionMode actionMode, int i, long l, boolean b) {

    }

    //Creates the menu within the timeline when the long press is implemented.
    @Override
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu)
    {
        MenuInflater inflater = actionMode.getMenuInflater();
        inflater.inflate(R.menu.timeline_context, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
        return false;
    }


    //This is the long press implementation that allows a tweet to be deleted, calls delete tweet below.
    @Override
    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem)
    {
        switch (menuItem.getItemId())
        {
            case R.id.menu_item_delete_tweet:
                deleteTweet(actionMode);
                return true;
            default:
                return false;
        }

    }

    //Method called for when the bin is pressed on the long delete, removes tweets from the array list in portfolio.
    private void deleteTweet(ActionMode actionMode)
    {
        for (int i = adapter.getCount() - 1; i >= 0; i--) {
            if (listView.isItemChecked(i)) {
                Tweet tweet = adapter.getItem(i);
                portfolio.deleteTweets(tweet);
                deleteTweet(actionMode);
            }
        }
        actionMode.finish();
        adapter.notifyDataSetChanged();
    }



    @Override
    public void onDestroyActionMode(ActionMode actionMode) {

    }

    @Override
    public void onResponse(Call<List<Tweet>> call, Response<List<Tweet>> response) {
        portfolio.tweets = (ArrayList<Tweet>) response.body();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onFailure(Call<List<Tweet>> call, Throwable t) {
        Toast toast = Toast.makeText(getActivity(), "Error retrieving tweets", Toast.LENGTH_LONG);

        toast.show();
    }

    class TimelineAdapter extends ArrayAdapter<Tweet>
    {

        private Context context;
        public List<Tweet> tweets;
        //private ArrayList<Tweet> tweets;

        public TimelineAdapter(Context context, ArrayList<Tweet> tweets) {
            super(context, 0, tweets);
            this.context = context;
            this.tweets = tweets;

        }

        @SuppressLint("InflateParams")
        @Override
        //Creates the timeline list with the content and time the tweet was sent.
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.timeline_list_item, null);
            }
            Tweet tweet = tweets.get(position);


            TextView content = (TextView) convertView.findViewById(R.id.time_line_list_content);
            TextView dateTextView = (TextView) convertView.findViewById(R.id.time_line_list_item_dateView);

            content.setText(tweet.content);
            dateTextView.setText(tweet.getDateString());

            return convertView;
        }

        @Override
        public int getCount() {
            return tweets.size();
        }
    }

   /* public void deleteTweet(Tweet tweet) {
        DeleteRemoteTweet delTweet = new DeleteRemoteTweet();
        Call<Tweet> call = app.tweetService.deleteTweet(tweet);
        call.enqueue(getActivity());
        Log.v("Tweet id",  id);

    }*/

    class DeleteRemoteTweet implements Callback<Tweet> {

        @Override
        public void onResponse(Call<Tweet> call, Response<Tweet> response) {
            Toast.makeText(getActivity(), "Tweet deleted", Toast.LENGTH_LONG).show();
            adapter.notifyDataSetChanged();
        }

        @Override
        public void onFailure(Call<Tweet> call, Throwable t) {
            Toast.makeText(getActivity(), "Failed to delete tweet", Toast.LENGTH_LONG).show();
        }
    }

    class RetrieveTweets implements Callback<List<Tweet>> {


        @Override
        public void onResponse(Call<List<Tweet>> call, Response<List<Tweet>> response) {
            List<Tweet> listTwt = response.body();
            Toast.makeText(getActivity(), "Retrieved " + listTwt.size() + " tweets", Toast.LENGTH_SHORT).show();
            portfolio.refreshTweets(listTwt);
            ((TweetingPagerAdapter) getListAdapter()).notifyDataSetChanged();
        }

        @Override
        public void onFailure(Call<List<Tweet>> call, Throwable t) {
            Toast.makeText(getActivity(), "Failed to retrieve tweet list", Toast.LENGTH_SHORT).show();
        }
    }
}
