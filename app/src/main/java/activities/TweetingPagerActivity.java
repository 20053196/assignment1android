package activities;

import android.os.Bundle;
import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;

import app.myTweetApp;
import models.Portfolio;
import models.Tweet;
import mytweet.org.wit.mytweet.R;

import static android.helpers.LogHelpers.info;

public class TweetingPagerActivity extends AppCompatActivity implements  ViewPager.OnPageChangeListener
{
    private ViewPager viewPager;
    private ArrayList<Tweet> tweets;
    private Portfolio portfolio;
    private PagerAdapter pagerAdapter;


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        viewPager = new ViewPager(this);
        viewPager.setId(R.id.viewPager);
        setContentView(viewPager);
        setTweetList();
        pagerAdapter = new TweetingPagerAdapter(getFragmentManager(), tweets);

        viewPager.setAdapter(pagerAdapter);
        setCurrentItem();
        viewPager.addOnPageChangeListener(this);
    }
    private void setTweetList()
    {
        myTweetApp app = (myTweetApp) getApplication();
        portfolio = app.portfolio;
        tweets = portfolio.tweets;
    }

    /*
  * Ensure selected tweets are shown in details view
  */
    private void setCurrentItem() {
        String twtId = (String) getIntent().getSerializableExtra(TweetingFragment.EXTRA_TWEET_ID);
        for (int i = 0; i < tweets.size(); i++) {
            if (tweets.get(i)._id.equals(twtId)) {
                viewPager.setCurrentItem(i);
                break;
            }
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        info(this, "onPageScrolled: position " + position + " arg1 " + positionOffset + " positionOffsetPixels " + positionOffsetPixels);
        Tweet tweet = tweets.get(position);
        if (tweet.content != null) {
            setTitle(tweet.content);
        }
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}

class TweetingPagerAdapter extends FragmentPagerAdapter
{
    private ArrayList<Tweet>  tweets;

    public TweetingPagerAdapter(FragmentManager fm, ArrayList<Tweet> tweets)
    {
        super(fm);
        this.tweets = tweets;
    }

    @Override
    public int getCount()
    {
        return tweets.size();
    }

    @Override
    public Fragment getItem(int pos)
    {
        Tweet tweet = tweets.get(pos);
        Bundle args = new Bundle();
        args.putSerializable(TweetingFragment.EXTRA_TWEET_ID, tweet._id);
        TweetingFragment fragment = new TweetingFragment();
        fragment.setArguments(args);
        return fragment;
    }
}