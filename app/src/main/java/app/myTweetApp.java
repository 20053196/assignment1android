package app;

import android.app.Application;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import models.Portfolio;
import models.Tweet;
import models.User;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class myTweetApp extends Application {

    public MyTweetService tweetService;
    public boolean tweetServiceAvailable = false;
    public String service_url= "http://mytweet-2.herokuapp.com";

   static final String TAG = "MyTweetApp";

    public static User currentUser;
    public List<Tweet> tweets = new ArrayList<Tweet>();
    public List<User> users = new ArrayList<User>();

    public Portfolio portfolio;
    protected static myTweetApp app;
    // Tweet temp tweet


    @Override
    public void onCreate()
    {
        super.onCreate();
        Gson gson = new GsonBuilder().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(service_url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        tweetService = retrofit.create(MyTweetService.class);
        portfolio = new Portfolio(getApplicationContext());
        Log.d(TAG, "MyTweet App started");
        app = this;
        currentUser = null;

    }

    public void newUser(User user) {
        users.add(user);
    }

    public static myTweetApp getApp(){
        return app;
    }

//Validates the credentials against the credntials in the arraylist.
    public boolean validUser(String email, String password)
    {
        for (User user : users)
        {
            if (user.email.equals(email) && user.password.equals(password))
            {
                currentUser =user;
                return true;
            }
        }
        return false;
    }


}

