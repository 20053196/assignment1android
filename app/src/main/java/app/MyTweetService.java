package app;
import java.util.List;

import models.Token;
import models.Tweet;
import models.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

//Available routes for the app to implement when using the API.
/**
 * Created by ictskills on 29/11/16.
 */
public interface MyTweetService {

    @GET("/api/tweets")
    Call<List<Tweet>> getAllTweets();

    @POST("/api/tweets/{id}/tweet")
    Call<Tweet> createTweet(@Path("id") String id, @Body Tweet tweet);

    @GET("/api/users")
    Call<List<User>> getAllUsers();

    @GET("/api/users/{id}")
    Call<User> getUser(@Path("id") String id);

    @POST("/api/users")
    Call<User> createUser(@Body User User);

    @POST("/api/tweets/tweet")
    Call<Tweet> deleteTweet(@Body Tweet tweet);




}
