package android.helpers;

import android.app.Application;
import android.util.Log;

import models.Portfolio;


public class LogHelpers extends Application
{
    public Portfolio portfolio;
    private static final String FILENAME = "portfolio.json";

    public static void info(Object parent, String message)
    {
        Log.i(parent.getClass().getSimpleName(), message);
    }

    @Override
    public void onCreate()
    {
        super.onCreate();

        info(this, "MyTweet app launched");
    }
}