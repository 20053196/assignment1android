package models;

import android.content.Context;
import android.widget.EditText;

import java.util.Date;
import java.util.Random;
import org.json.JSONException;
import org.json.JSONObject;

import mytweet.org.wit.mytweet.R;

/**
 * Created by ictskills on 04/10/16.
 * Tweet model for creating new tweet objects.
 */
public class Tweet {

    public String content;
    public Long date;
    public String _id;
    public String photo;
    public String firstName;

    public Tweet()
    {
        this._id = null;
        // _id = String.valueOf(unsignedLong());
        //new Random().nextLong().toString();
        date = new Date().getTime();
        photo = "photo";
        firstName = _id;

    }

    public void setContent(String content)
    {
        this.content = content;
    }
    public String getContent()

    {
        return content;
    }
    public String getDateString()
    {
        return dateString();
    }
    private String dateString()
    {
        String dateFormat = "EEE d MMM yyyy H:mm";
        return android.text.format.DateFormat.format(dateFormat, date).toString();
    }



    /*private Long unsignedLong() {
        long rndVal = 0;
        do {
            rndVal = new Random().nextLong();
        } while (rndVal <= 0);
        return rndVal;
    }*/



}