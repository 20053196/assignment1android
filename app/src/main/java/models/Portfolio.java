package models;

/**
 * Created by ictskills on 03/10/16.
 */
import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import sqlite.dbhelper;

import static android.helpers.LogHelpers.info;

public class Portfolio {

    public ArrayList<Tweet> tweets;
    public ArrayList<User> users;
    public dbhelper dbHelper;

    public Portfolio(Context context) {
        try {
            dbHelper = new dbhelper(context);
            tweets = (ArrayList<Tweet>) dbHelper.selectTweets();
           // users = (ArrayList<User>) dbHelper.selectUsers();
        }
        catch (Exception e) {
            info(this, "Error loading tweets: " + e.getMessage());
            tweets = new ArrayList<Tweet>();
            users = new ArrayList<User>();
        }
    }

    /**
     * Obtain the entire database of tweets
     *
     * @return All the tweets in the database as an ArrayList
     */
       public ArrayList<Tweet> selectTweets() {
        return (ArrayList<Tweet>) dbHelper.selectTweets();
    }

    /*//  public ArrayList<User> selectUsers() {
        return (ArrayList<User>)dbHelper.selectUsers();
    }*/


    public Tweet getTweet(String id) {
        Log.i(this.getClass().getSimpleName(), "String parameter id: "+ id);

        for(Tweet t: tweets)
        {
            if(id.equals(t._id)){
            return t;
        }
    }
        return null;
}

    public User getUser(String id) {
        Log.i(this.getClass().getSimpleName(), "String id: " + id);

        for (User user : users)
        {
            if (id.equals(user._id)) {
                return user;
            }
        }
        info(this, "failed to find user. returning first element array to avoid crash");
        return null;
    }


    public void addTweet(Tweet tweet){
        tweets.add(tweet);
        dbHelper.addTweets(tweet);
    }

  /*  public void addUser(User user) {
        users.add(user);
        dbHelper.addUser(user);
    }

*/
    public void clearTweets()
    {
        tweets.clear();
        dbHelper.deleteTweets();

    }

    public void clearUsers()
    {
        users.clear();
      //  dbHelper.deleteUsers();
    }

    public void deleteTweets(Tweet tweet)
    {
        dbHelper.deleteTweet(tweet);
        tweets.remove(tweet);
    }

    public void deleteUsers(User user)
    {
     //   dbHelper.deleteUser(user);
        users.remove(user);
    }

    public void updateTweets(Tweet tweet) {
        dbHelper.updateTweets(tweet);
        updateLocalTweets(tweet);

    }

    public void refreshTweets(List<Tweet> tweets)
    {
        dbHelper.deleteTweets();
        this.tweets.clear();

        dbHelper.addTweets(tweets);

        for (int i = 0; i < tweets.size(); i += 1) {
            this.tweets.add(tweets.get(i));
        }
    }

    /**
     * Search the list of tweets for argument tweet
     * If found replace it with argument tweet.
     * If not found just add the argument tweet.
     *

     */
    private void updateLocalTweets(Tweet tweet) {
        for (int i = 0; i < tweets.size(); i += 1) {
            Tweet t = tweets.get(i);
            if (t._id.equals(tweet._id)) {
                tweets.remove(i);
                tweets.add(tweet);
                return;
            }
        }
    }
    
}




