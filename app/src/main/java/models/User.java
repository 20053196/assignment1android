package models;

//User model for creating new user objects.
public class User {
    public String _id;
    public String firstName;
    public String lastName;
    public String email;
    public String password;

    public User(String firstName, String lastName, String email, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
    }

    public User()
    {
        this.firstName = "";
        this.lastName ="";
        this.email ="";
        this.password = "";


    }


}
