package settings;

import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.MenuItem;


import static android.helpers.IntentHelper.navigateUp;
import static android.helpers.LogHelpers.info;

import mytweet.org.wit.mytweet.R;


/**
 * Created by ictskills on 21/10/16.
 */

    public class SettingsFragment extends PreferenceFragment  implements SharedPreferences.OnSharedPreferenceChangeListener {

        private SharedPreferences prefs;


        @Override
        public void onCreate(Bundle savedInstanceState)  {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.settings);
            setHasOptionsMenu(true);
        }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                navigateUp(getActivity());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

        @Override
        public void onStart()
        {
            super.onStart();
            prefs = PreferenceManager
                    .getDefaultSharedPreferences(getActivity());

            prefs.registerOnSharedPreferenceChangeListener(this);
        }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        info(getActivity(), "Setting change - key : value = " + key + " : " + sharedPreferences.getString(key, ""));
    }

    @Override
    public void onStop()
    {
        super.onStop();
        prefs.unregisterOnSharedPreferenceChangeListener(this);
    }


}


