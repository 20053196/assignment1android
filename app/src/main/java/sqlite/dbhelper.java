package sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


import java.util.ArrayList;
import java.util.List;

import models.Tweet;
import models.User;


public class dbhelper extends SQLiteOpenHelper
{
    static final String TAG = "dbhelper";
    static final String DATABASE_NAME = "tweets.db";
    static final int DATABASE_VERSION = 1;

    static final String TABLE_TWEETS = "tableTweets";
    static final String PRIMARY_KEY = "id";
    static final String DATE = "date";
    static final String CONTENT = "content";

  /*  static final String TABLE_USERS = "tableUsers";
    static final String USERPRIMARY_KEY = "_id";
    static final String FIRSTNAME = "firstName";
    static final String LASTNAME = "lastName";
    static final String EMAIL = "email";
    static final String PASSWORD = "password";*/


    Context context;

    public dbhelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable =
                "CREATE TABLE tableTweets " +
                        "(id text primary key, " +
                        "content text, " +
                        "date text) ";

       /* String tableUsers =
               "CREATE TABLE tableUsers " +
                        "(_id text user primary key, " +
                        "firstName text, " +
                        "lastName text, " +
                        "email text, " +
                        "password text)";
*/
        db.execSQL(createTable);
       /* db.execSQL(tableUsers);*/
        Log.d(TAG, "DbHelper.onCreated: " + createTable/* + tableUsers*/);
    }

/*Add tweets to a list*/
    public void addTweets(Tweet tweet) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(PRIMARY_KEY, tweet._id);
        values.put(CONTENT, tweet.content);
        values.put(DATE, tweet.date);
        // Insert record
        db.insert(TABLE_TWEETS, null, values);
        db.close();
    }

   /* public void addUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(USERPRIMARY_KEY, user._id);
        values.put(FIRSTNAME, user.firstName);
        values.put(LASTNAME, user.lastName);
        values.put(EMAIL,user.email);
        values.put(PASSWORD, user.password);
        //Insert record
        db.insert(TABLE_USERS, null, values);
        db.close();
    }*/
   /* public User selectUser(String _id ) {
        User user;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;

        try {
            user = new User();

            cursor = db.rawQuery("SELECT * FROM tableUsers WHERE _id = ?", new String[]{user._id.toString() + ""});

            if (cursor.getCount() > 0) {
                int columnIndex = 0;
                cursor.moveToFirst();
                user._id = cursor.getString(columnIndex++);
                user.firstName = cursor.getString(columnIndex++);
                user.lastName = cursor.getString(columnIndex++);
                user.email = cursor.getString(columnIndex++);
                user.password = cursor.getString(columnIndex++);
            }
        }
        finally {
            cursor.close();
        }
        return user;
    }*/




    /**
     * Persist a list of tweets
     *
     */
    public void addTweets(List<Tweet> tweets) {
        for (Tweet tweet : tweets) {
            addTweets(tweet);
        }
    }


    /**
     * Persist a list of users
     *
     */
  /*  public void addUsers(List<User> users) {
        for (User user : users) {
            addUser(user);
        }
    }*/


    public void deleteTweet(Tweet tweet) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.delete("tableTweets", "id" + "=?", new String[]{tweet._id + ""});
        }
        catch (Exception e) {
            Log.d(TAG, "delete tweet failure: " + e.getMessage());
        }
    }


   /* public void deleteUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.delete("tableUsers", "id" + "=?", new String[]{user._id + ""});
        }
        catch (Exception e) {
            Log.d(TAG, "delete user failure: " + e.getMessage());
        }
    }*/
   /* public List<User> selectUsers(){
        List<User> users = new ArrayList<>();
        String query = "SELECT * FROM " + "tableUsers";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            int columnIndex = 0;
            do {
                User user = new User();
                user._id = cursor.getString(columnIndex++);
                user.firstName = cursor.getString(columnIndex++);
                user.lastName = cursor.getString(columnIndex++);
                user.email = cursor.getString(columnIndex++);
                user.password = cursor.getString(columnIndex++);
                columnIndex = 0;

                users.add(user);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return users;
    }
*/
    /**
     * Query database and select entire tableTweets.
     *
     * @return A list of Tweet object records
     */
    public List<Tweet> selectTweets() {

        List<Tweet> tweets = new ArrayList<Tweet>();
        String query = "SELECT * FROM " + "tableTweets";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            int columnIndex = 0;
            do {
                Tweet tweet = new Tweet();
                tweet._id = cursor.getString(columnIndex++);
                tweet.content = cursor.getString(columnIndex++);
                tweet.date = Long.parseLong(cursor.getString(columnIndex++));
                columnIndex = 0;

                tweets.add(tweet);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return tweets;
    }

    /**
     * Delete all records
     */
    public void deleteTweets() {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.execSQL("delete from tableTweets");
        }
        catch (Exception e) {
            Log.d(TAG, "delete tweets failure: " + e.getMessage());
        }
    }

    /*//Delete all users
    public void deleteUsers() {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.execSQL("delete from tableUsers");
        }
        catch (Exception e) {
            Log.d(TAG, "delete user failure: " + e.getMessage());
        }
    }*/

    /**
     * Queries the database for the number of records.
     *
     * @return The number of records in the database.
     */
    public long getCount() {
        SQLiteDatabase db = this.getReadableDatabase();
        long numberRecords = DatabaseUtils.queryNumEntries(db, TABLE_TWEETS);
        db.close();
        return numberRecords;
    }

    /**
     * Update an existing Tweet record.
     * All fields except record id updated.
     *
     */
    public void updateTweets(Tweet tweet) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(CONTENT, tweet.content);
            values.put(DATE, tweet.date);
            db.update("tableTweets", values, "id" + "=?", new String[]{tweet._id + ""});
        }
        catch (Exception e) {
            Log.d(TAG, "update tweets failure: " + e.getMessage());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + TABLE_TWEETS);
        Log.d(TAG, "onUpdated");
        onCreate(db);
    }




}